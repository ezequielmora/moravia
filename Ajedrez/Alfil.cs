﻿using System;
using System.Collections.Generic;

using Ajedrez.Tablero;
using Ajedrez.Tablero.Excepciones;
using AjedrezMoravia.TableroDeJuego;

namespace AjedrezMoravia.Ajedrez
{
    sealed class Alfil : Pieza
    {
        public Alfil (Tablero tablero, Color color) 
            : base(tablero, color)
        {
        }

        public override bool[,] MovimientosPosibles ()
        {
            List<Posicion> posiciones = new List<Posicion>();
            bool[,] movements = new bool[Tablero.Lineas, Tablero.Columnas];
            var loopCount = 1;

            for (var i = Posicion.Columna; i < Tablero.Columnas; i++ )
            {
                var posicion = new Posicion(Posicion.Linea - loopCount, Posicion.Columna + loopCount);
                TesteoDeMovimiento(posicion, posiciones);

                if (Tablero.EsUnaPosicionValida(posicion) && Tablero.PiezaExistente(posicion))
                    break;

                loopCount++;
            }

            loopCount = 1;

            for (var c = Posicion.Columna; c >= 0; c--)
            {
                var posicion = new Posicion(Posicion.Linea - loopCount, Posicion.Columna - loopCount);
                TesteoDeMovimiento(posicion, posiciones);

                if (Tablero.EsUnaPosicionValida(posicion) && Tablero.PiezaExistente(posicion))
                    break;

                loopCount++;
            }

            loopCount = 1;

            for (var i = Posicion.Columna; i >= 0; i--)
            {
                var posicion = new Posicion(Posicion.Linea + loopCount, Posicion.Columna - loopCount);
                TesteoDeMovimiento(posicion, posiciones);

                if (Tablero.EsUnaPosicionValida(posicion) && Tablero.PiezaExistente(posicion))
                    break;

                loopCount++;
            }

            loopCount = 1;

            for (var c = Posicion.Columna; c < Tablero.Columnas; c++)
            {
                var position = new Posicion(Posicion.Linea + loopCount, Posicion.Columna + loopCount);
                TesteoDeMovimiento(position, posiciones);

                if (Tablero.EsUnaPosicionValida(position) && Tablero.PiezaExistente(position))
                    break;

                loopCount++;
            }

            foreach(var currentMovement in posiciones)
                movements[currentMovement.Linea, currentMovement.Columna] = true;

            return movements;
        }
        
        // Testea los movimientos posibles del alfil
        private void TesteoDeMovimiento (Posicion posicion, List<Posicion> posiciones)
        {
            if (Tablero.EsUnaPosicionValida(posicion) && PuedeMover(posicion))
                posiciones.Add(posicion);
        }

        public override string ToString ()
        {
            return "A";
        }
    }
}
