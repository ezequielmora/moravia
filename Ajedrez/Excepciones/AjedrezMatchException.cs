﻿using System;

namespace AjedrezMoravia.Ajedrez.Excepciones
{
    class AjedrezMatchException : Exception
    {
        public AjedrezMatchException(string message) : base(message)
        {

        }
    }
}
