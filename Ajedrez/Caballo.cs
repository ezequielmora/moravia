﻿using System;
using System.Collections.Generic;
using AjedrezMoravia.TableroDeJuego;

namespace AjedrezMoravia.Ajedrez
{
    sealed class Caballo : Pieza
    {
        public Caballo (Tablero chessBoard, Color color)
            : base(chessBoard, color)
        {
        }

        public override bool[,] MovimientosPosibles ()
        {
            bool[,] movements = new bool[Tablero.Lineas, Tablero.Columnas];
            Posicion[] posiciones = new Posicion[]
            {
                new Posicion(Posicion.Linea - 2, Posicion.Columna - 1),
                new Posicion(Posicion.Linea - 2, Posicion.Columna + 1),
                new Posicion(Posicion.Linea + 2, Posicion.Columna - 1),
                new Posicion(Posicion.Linea + 2, Posicion.Columna + 1),
                new Posicion(Posicion.Linea - 1, Posicion.Columna + 2),
                new Posicion(Posicion.Linea + 1, Posicion.Columna + 2),
                new Posicion(Posicion.Linea - 1, Posicion.Columna - 2),
                new Posicion(Posicion.Linea + 1, Posicion.Columna - 2)
            };

            foreach (var posicionActual in posiciones)
                if (Tablero.EsUnaPosicionValida(posicionActual) && PuedeMover(posicionActual))
                    movements[posicionActual.Linea, posicionActual.Columna] = true;

            return movements;

        }

        public override string ToString ()
        {
            return "C";
        }
    }   
}
