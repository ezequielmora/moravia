﻿using AjedrezMoravia.TableroDeJuego;
using System;
using System.Collections.Generic;


namespace AjedrezMoravia.Ajedrez
{
    sealed class Torre : Pieza
    {
        public Torre (Tablero tablero, Color color)
            : base(tablero, color)
        {
        }

        public override bool[,] MovimientosPosibles ()
        {
            List<Posicion> Posicions = new List<Posicion>();
            bool[,] movements = new bool[Tablero.Columnas, Tablero.Lineas];

            // Left movement
            for (int c = Posicion.Columna - 1; c >= 0;  c--)
            {
                var posicion = new Posicion(Posicion.Linea, c);
                TestMovement(posicion, Posicions);

                if (Tablero.PiezaExistente(posicion))
                    break;
            }
            // Right movement
            for (int c = Posicion.Columna + 1; c < 8; c++)
            {
                var posicion = new Posicion(Posicion.Linea, c);
                TestMovement(posicion, Posicions);

                if (Tablero.PiezaExistente(posicion))
                    break;
            }
            // Up movement
            for (int l = Posicion.Linea - 1; l >= 0; l--)
            {
                var posicion = new Posicion(l, Posicion.Columna);
                TestMovement(posicion, Posicions);

                if (Tablero.PiezaExistente(posicion))
                    break;
            }
            // Down movement
            for (int l = Posicion.Linea + 1; l < 8; l++)
            {
                var posicion = new Posicion(l, Posicion.Columna);
                TestMovement(posicion, Posicions);

                if (Tablero.PiezaExistente(posicion))
                    break;
            }

            foreach (var currentPosicion in Posicions)
                movements[currentPosicion.Linea, currentPosicion.Columna] = true;

            return movements;
        }

        private void TestMovement (Posicion Posicion, List<Posicion> Posicions)
        {
            if (PuedeMover(Posicion))
                Posicions.Add(Posicion);
        }

        public override string ToString ()
        {
            return "T";
        }
    }
}
