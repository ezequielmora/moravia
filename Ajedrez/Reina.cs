﻿using System.Collections.Generic;
using Ajedrez.Tablero;
using Ajedrez.Tablero.Excepciones;
using AjedrezMoravia.TableroDeJuego;

namespace AjedrezMoravia.Ajedrez
{
    sealed class Reina : Pieza
    {
        public Reina (Tablero chessBoard, Color color) 
            : base(chessBoard, color)
        {
        }

        public override bool[,] MovimientosPosibles ()
        {
            List<Posicion> posiciones = new List<Posicion>();
            bool[,] movimientos = new bool[Tablero.Columnas, Tablero.Lineas];
            var bucles = 1;

            // Left movement
            for (int i = Posicion.Columna - 1; i >= 0; i--)
            {
                var posicion = new Posicion(Posicion.Linea, i);
                TestMovement(posicion, posiciones);

                if (Tablero.PiezaExistente(posicion))
                    break;
            }
            // Right movement
            for (int i = Posicion.Columna + 1; i < 8; i++)
            {
                var position = new Posicion(Posicion.Linea, i);
                TestMovement(position, posiciones);

                if (Tablero.PiezaExistente(position))
                    break;
            }
            // Up movement
            for (int l = Posicion.Linea - 1; l >= 0; l--)
            {
                var position = new Posicion(l, Posicion.Columna);
                TestMovement(position, posiciones);

                if (Tablero.PiezaExistente(position))
                    break;
            }
            // Down movement
            for (int l = Posicion.Linea + 1; l < 8; l++)
            {
                var position = new Posicion(l, Posicion.Columna);
                TestMovement(position, posiciones);
                
                if (Tablero.PiezaExistente(position))
                    break;
            }

            for (var c = Posicion.Columna; c < Tablero.Columnas; c++)
            {
                var position = new Posicion(Posicion.Linea - bucles, Posicion.Columna + bucles);
                TestMovement(position, posiciones);

                if (Tablero.EsUnaPosicionValida(position) && Tablero.PiezaExistente(position))
                    break;

                bucles++;
            }

            bucles = 1;

            for (var c = Posicion.Columna; c >= 0; c--)
            {
                var position = new Posicion(Posicion.Linea - bucles, Posicion.Columna - bucles);
                TestMovement(position, posiciones);

                if (Tablero.EsUnaPosicionValida(position) && Tablero.PiezaExistente(position))
                    break;

                bucles++;
            }

            bucles = 1;

            for (var i = Posicion.Columna; i >= 0; i--)
            {
                var position = new Posicion(Posicion.Linea + bucles, Posicion.Columna - bucles);
                TestMovement(position, posiciones);

                if (Tablero.EsUnaPosicionValida(position) && Tablero.PiezaExistente(position))
                    break;
                
                bucles++;
            }

            bucles = 1;

            for (var i = Posicion.Columna; i < Tablero.Columnas; i++)
            {
                var posicion = new Posicion(Posicion.Linea + bucles, Posicion.Columna + bucles);
                TestMovement(posicion, posiciones);

                if (Tablero.EsUnaPosicionValida(posicion) && Tablero.PiezaExistente(posicion))
                    break;

                bucles++;
            }

            foreach (var movimientoActual in posiciones)
                movimientos[movimientoActual.Linea, movimientoActual.Columna] = true;

            return movimientos;
        }

        private void TestMovement (Posicion posicion, List<Posicion> posiciones)
        {
            if (Tablero.EsUnaPosicionValida(posicion) && PuedeMover(posicion))
                posiciones.Add(posicion);
        }

        public override string ToString ()
        {
            return "Q";
        }
    }
}
