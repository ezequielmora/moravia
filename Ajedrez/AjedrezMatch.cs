﻿using System;
using System.Collections.Generic;

using AjedrezMoravia.TableroDeJuego;
using Ajedrez.Tablero.Excepciones;
using AjedrezMoravia.Ajedrez.Excepciones;

namespace AjedrezMoravia.Ajedrez
{
    class AjedrezMatch
    {
        public Tablero Tablero
        {
            get;
            private set;
        }
        public int Turno
        {
            get;
            private set;
        }
        public Color JugadorActual
        {
            get;
            private set;
        }
        public bool Finalizado
        {
            get;
            private set;
        }
        public List<Pieza> PiezasEnJuego
        {
            get;
            private set;
        }
        public List<Pieza> PiezasFueraDeJuego
        {
            get;
            private set;
        }
        public DateTime EmpiezaEn
        {
            get;
            private set;
        }
        public DateTime FinalizaEn
        {
            get;
            private set;
        }
        public bool Check
        {
            get;
            private set;
        }

        public AjedrezMatch()
        {
            Tablero = new Tablero();
            Turno = 1;
            JugadorActual = Color.Blanco;
            Finalizado = false;
            EmpiezaEn = DateTime.Now;
            PiezasEnJuego = new List<Pieza>();
            PiezasFueraDeJuego = new List<Pieza>();

            PosicionarPiezas();
        }

        public void EjecutarMovimiento(PosicionDelTablero origen, PosicionDelTablero destino)
        {
            var originPiece = Tablero.ObtenerPieza(origen.HaciaPosicion());

            if (originPiece.esPosibleElMovimiento(destino.HaciaPosicion()))
            {
                RemoverPieza(origen);
                var currentTargetPiece = Tablero.ObtenerPieza(destino.HaciaPosicion());

                if (currentTargetPiece != null)
                    RemoverPieza(destino, currentTargetPiece);

                InsertarPieza(originPiece, destino);

                // Verifica si el juegador no esta en jaque
                if (EstaEnJaque(ObtenerRey(JugadorActual)))
                {
                    DezhacerMovimiento(origen, destino, currentTargetPiece);

                    if (Check)
                        throw new AjedrezMatchException("Has sido puesto en Jaque.");
                    else
                        throw new AjedrezMatchException("No puedes ponerte en jaque a ti mismo.");
                }

                // Verifica si el jugador hizo jaque al rival
                Check = EstaEnJaque(ObtenerRey(Rival(JugadorActual)));

                // Verifica si el jugador hizo jaque mate al rival
                if (Check)
                {
                    if (EstaEnJaqueMate(ObtenerRey(Rival(JugadorActual))))
                    {
                        Finalizado = true;
                        FinalizaEn = DateTime.Now;
                    }
                }

                SiguienteTurno(originPiece);
            }
            else
            {
                throw new AjedrezMatchException("Esta pieza no puede realizar ese movimiento.");
            }
        }

        private void DezhacerMovimiento(PosicionDelTablero origen, PosicionDelTablero destino, Pieza removedPiece)
        {
            var oldOriginPiece = Tablero.ObtenerPieza(destino.HaciaPosicion());

            RemoverPieza(destino);
            InsertarPieza(oldOriginPiece, origen);
            oldOriginPiece.DecrementarMovimiento();

            if (removedPiece != null)
                InsertarNuevaPieza(removedPiece, destino);
        }

        public void ChequearPosicionDeOrigen(PosicionDelTablero origin)
        {
            if (Tablero.PiezaExistente(origin.HaciaPosicion()))
            {
                if (!Tablero.ObtenerPieza(origin.HaciaPosicion()).Color.Equals(JugadorActual))
                    throw new AjedrezMatchException("Unicamente puedes mover tus propias piezas.");
            }
            else
            {
                throw new ExcepcionTablero("No existe una pieza en esta posicion.");
            }
        }

        public void ChequearPosicionDeDestino(PosicionDelTablero target)
        {
            Tablero.PosicionValida(target.HaciaPosicion());
        }

        public List<Pieza> ObtenerPiezasEnJuego(Color color)
        {
            return PiezasEnJuego.FindAll(p => p.Color.Equals(color));
        }

        public List<Pieza> ObtenerPiezasFueraDeJuego(Color color)
        {
            return PiezasFueraDeJuego.FindAll(p => p.Color.Equals(color));
        }

        private void PosicionarPiezas()
        {
            // Piezas blancas
            InsertarNuevaPieza(new Torre(Tablero, Color.Blanco), PosicionAleatoria());
            InsertarNuevaPieza(new Torre(Tablero, Color.Blanco), PosicionAleatoria());
            InsertarNuevaPieza(new Rey(Tablero, Color.Blanco), PosicionAleatoria());
            InsertarNuevaPieza(new Reina(Tablero, Color.Blanco), PosicionAleatoria());
            InsertarNuevaPieza(new Alfil(Tablero, Color.Blanco), PosicionAleatoria());
            InsertarNuevaPieza(new Alfil(Tablero, Color.Blanco), PosicionAleatoria());
            InsertarNuevaPieza(new Caballo(Tablero, Color.Blanco), PosicionAleatoria());
            InsertarNuevaPieza(new Caballo(Tablero, Color.Blanco), PosicionAleatoria());

            // Peones Blancos
            for (var i = 0; i < Tablero.Columnas; i++)
            {
                char currentColumn = (char)('a' + i);
                InsertarNuevaPieza(new Peon(Tablero, Color.Blanco), PosicionAleatoria());
            }

            // Piezas negras
            InsertarNuevaPieza(new Torre(Tablero, Color.GrisOscuro), PosicionAleatoria());
            InsertarNuevaPieza(new Torre(Tablero, Color.GrisOscuro), PosicionAleatoria());
            InsertarNuevaPieza(new Rey(Tablero, Color.GrisOscuro), PosicionAleatoria());
            InsertarNuevaPieza(new Reina(Tablero, Color.GrisOscuro), PosicionAleatoria());
            InsertarNuevaPieza(new Alfil(Tablero, Color.GrisOscuro), PosicionAleatoria());
            InsertarNuevaPieza(new Alfil(Tablero, Color.GrisOscuro), PosicionAleatoria());
            InsertarNuevaPieza(new Caballo(Tablero, Color.GrisOscuro), PosicionAleatoria());
            InsertarNuevaPieza(new Caballo(Tablero, Color.GrisOscuro), PosicionAleatoria());

            // Peones negros
            for (var i = 0; i < Tablero.Columnas; i++)
            {
                char columnaActual = (char)('a' + i);
                InsertarNuevaPieza(new Peon(Tablero, Color.GrisOscuro), PosicionAleatoria());
            }
        }

        private void InsertarPieza(Pieza piece, PosicionDelTablero posicionTablero)
        {
            Tablero.PonerPieza(piece, posicionTablero.HaciaPosicion());
        }

        private void InsertarNuevaPieza(Pieza pieza, PosicionDelTablero posicionTablero)
        {
            Tablero.PonerPieza(pieza, posicionTablero.HaciaPosicion());

            PiezasFueraDeJuego.Remove(pieza);
            PiezasEnJuego.Add(pieza);
        }

        private void RemoverPieza(PosicionDelTablero posicionTablero)
        {
            Tablero.RemoverPieza(posicionTablero.HaciaPosicion());
        }

        private void RemoverPieza(PosicionDelTablero posicionTablero, Pieza piece)
        {
            RemoverPieza(posicionTablero);

            PiezasEnJuego.Remove(piece);
            PiezasFueraDeJuego.Add(piece);
        }

        private void SiguienteTurno(Pieza piezaDeOrigen)
        {
            piezaDeOrigen.IncrementarMovimiento();

            if (!Finalizado)
            {
                JugadorActual = (JugadorActual.Equals(Color.Blanco)) ? Color.GrisOscuro : Color.Blanco;
                Turno++;
            }
        }

        private bool EstaEnJaque(Rey king)
        {
            var piezasDelRival = ObtenerPiezasEnJuego(Rival(king.Color));

            foreach (var piezaActual in piezasDelRival)
                if (piezaActual.esPosibleElMovimiento(king.Posicion))
                    return true;

            return false;
        }

        private bool EstaEnJaqueMate(Rey king)
        {
            bool[,] PosibleMovimientosDelRey = king.MovimientosPosibles();
            var cuentaDePosibleMovimientosDelRey = 0;

            foreach (var currentPiece in ObtenerPiezasEnJuego(Rival(king.Color)))
            {
                bool[,] posiblesMovimientosDelRival = currentPiece.MovimientosPosibles();

                for (var i = 0; i < Tablero.Lineas; i++)
                    for (var x = 0; x < Tablero.Columnas; x++)
                        if (PosibleMovimientosDelRey[i, x] && posiblesMovimientosDelRival[i, x])
                            PosibleMovimientosDelRey[i, x] = false;
            }

            for (var l = 0; l < Tablero.Lineas; l++)
                for (var c = 0; c < Tablero.Columnas; c++)
                    if (PosibleMovimientosDelRey[l, c])
                        cuentaDePosibleMovimientosDelRey++;

            return (cuentaDePosibleMovimientosDelRey.Equals(0));
        }

        private Rey ObtenerRey(Color color)
        {
            return (Rey)ObtenerPiezasEnJuego(color).Find(p => p is Rey);
        }

        public static Color Rival(Color color)
        {
            return (color.Equals(Color.Blanco)) ? Color.GrisOscuro : Color.Blanco;
        }

        // matriz de 8 x 8 vacia
        private Dictionary<string, PosicionDelTablero> PanelTemporal = new Dictionary<string, PosicionDelTablero>();

        private PosicionDelTablero PosicionAleatoria()
        {

            PosicionDelTablero posicionEncontrada;

            // por cada pieza hago un randon de letra (columna y fila)
            char columna = ObtenerLetraRandom();
            int fila = ObtenerNumeroRandom();

            while (true)
            {
                // si la ubicacion esta libre la seteo si no hago otro random hasta podes guardarlo
                if (!PanelTemporal.ContainsKey(columna + "-" + fila))
                {
                    posicionEncontrada = new PosicionDelTablero(columna, fila);
                    PanelTemporal.Add(columna + "-" + fila, posicionEncontrada);

                    break;
                }
                else
                {
                    columna = ObtenerLetraRandom();
                    fila = ObtenerNumeroRandom();
                }
            }

            // retorno la posicion del tablero
            return posicionEncontrada;
        }

        static Random _random = new Random();
        public static char ObtenerLetraRandom()
        {
            // Devuelve letra al azar de a la a-h
            int num = _random.Next(0, 7);
            char let = (char)('a' + num);
            return let;
        }
        
        public static int ObtenerNumeroRandom()
        {
            // Devuelve letra al azar de a la a-h
            int num = _random.Next(1, 8);
            return num;
        }
    }
}
