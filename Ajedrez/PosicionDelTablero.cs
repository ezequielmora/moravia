﻿using System;

using AjedrezMoravia.TableroDeJuego;

namespace AjedrezMoravia.Ajedrez
{
    class PosicionDelTablero
    {
        public char Columna {
            get;
            private set;
        }
        public int Linea {
            get;
            private set;
        }
        
        public PosicionDelTablero (char columna, int linea)
        {
            Columna = columna;
            Linea = linea;
        }

        public Posicion HaciaPosicion ()
        {
            return new Posicion(Tablero.Lineas - Linea, Columna - 'a');
        }

        public override string ToString ()
        {
            return string.Concat(Columna, Linea);
        }
    }
}
