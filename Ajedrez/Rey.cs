﻿using System;
using AjedrezMoravia.TableroDeJuego;

namespace AjedrezMoravia.Ajedrez
{
    sealed class Rey : Pieza
    {
        public Rey (Tablero tablero, Color color) : base(tablero, color)
        {
        }

        public override bool[,] MovimientosPosibles ()
        {
            bool[,] movimientos = new bool[Tablero.Columnas, Tablero.Lineas];
            Posicion[] movimientoDeTest = new Posicion[]
            {
                new Posicion(Posicion.Linea + 1, Posicion.Columna),
                new Posicion(Posicion.Linea + 1, Posicion.Columna + 1),
                new Posicion(Posicion.Linea, Posicion.Columna + 1),
                new Posicion(Posicion.Linea - 1, Posicion.Columna + 1),
                new Posicion(Posicion.Linea - 1, Posicion.Columna),
                new Posicion(Posicion.Linea - 1, Posicion.Columna - 1),
                new Posicion(Posicion.Linea, Posicion.Columna - 1),
                new Posicion(Posicion.Linea + 1, Posicion.Columna - 1)
            };
            
            foreach (var movimientoActual in movimientoDeTest)
                if (Tablero.EsUnaPosicionValida(movimientoActual) && PuedeMover(movimientoActual))
                    movimientos[movimientoActual.Linea, movimientoActual.Columna] = true;

            return movimientos;
        }

        public override string ToString ()
        {
            return "R";
        }
    }
}
