﻿using System;
using System.Collections.Generic;
using AjedrezMoravia.TableroDeJuego;
using AjedrezMoravia.Ajedrez;

namespace AjedrezMoravia.Ajedrez
{
    sealed class Peon : Pieza   
    {
        public Peon (Tablero tablero, Color color)
            : base(tablero, color)
        {
        }

        public override bool[,] MovimientosPosibles ()
        {
            bool[,] movements = new bool[Tablero.Lineas, Tablero.Columnas];
            Posicion[] positions = new Posicion[4];

            if(Color.Equals(Color.Blanco))
            {
                positions[0] = (new Posicion(Posicion.Linea - 1, Posicion.Columna));
                positions[1] = Movimientos.Equals(0) ? (new Posicion(Posicion.Linea - 2, Posicion.Columna)) : null;
                positions[2] = (new Posicion(Posicion.Linea - 1, Posicion.Columna + 1));
                positions[3] = (new Posicion(Posicion.Linea - 1, Posicion.Columna - 1));
            }
            else
            {
                positions[0] = (new Posicion(Posicion.Linea + 1, Posicion.Columna));
                positions[1] = Movimientos.Equals(0) ? (new Posicion(Posicion.Linea + 2, Posicion.Columna)) : null;
                positions[2] = (new Posicion(Posicion.Linea + 1, Posicion.Columna + 1));
                positions[3] = (new Posicion(Posicion.Linea + 1, Posicion.Columna - 1));
            }

            for (var i = 0; i < positions.Length; i++ )
            {
                var posicionActual = positions[i];

                if (posicionActual != null && Tablero.EsUnaPosicionValida(posicionActual))
                {
                    if (i < 2)
                    {
                        if (PuedeMover(posicionActual))
                            movements[posicionActual.Linea, posicionActual.Columna] = true;
                    }
                    else
                    {
                        if (Tablero.ExisteEnemigo(posicionActual, AjedrezMatch.Rival(Color)))
                            movements[posicionActual.Linea, posicionActual.Columna] = true;
                    }
                }   
            }

            return movements;
        }

        protected override bool PuedeMover (Posicion position)
        {
            return !Tablero.PiezaExistente(position);
        }

        public override string ToString ()
        {
            return "P";
        }
    }
}
