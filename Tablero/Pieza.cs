﻿using System;

namespace AjedrezMoravia.TableroDeJuego
{
    abstract class Pieza
    {
        public Tablero Tablero {
            get;
            private set;
        }
        public Posicion Posicion {
            get;
            protected set;
        }
        public Color Color {
            get;
            protected set;
        }
        public int Movimientos {
            get;
            private set;
        }

        public Pieza (Tablero tablero, Color color)
        {
            Tablero = tablero;
            Color = color;

            Posicion = null;
            Movimientos = 0;
        }  
      
        public void IncrementarMovimiento ()
        {
            Movimientos++;
        }

        public void DecrementarMovimiento ()
        {
            Movimientos--;
        }

        public void CambiarPosicion (Posicion nuevaPosicion)
        {
            Posicion = nuevaPosicion;
        }

        public bool esPosibleElMovimiento (Posicion posicion)
        {
            return MovimientosPosibles()[posicion.Linea, posicion.Columna];
        }

        protected virtual bool PuedeMover (Posicion position)
        {
            return (!Tablero.PiezaExistente(position) ||  Tablero.ObtenerPieza(position).Color != Color);
        }

        public abstract bool[,] MovimientosPosibles ();
    }
}
