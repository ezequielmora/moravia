﻿using System;

namespace AjedrezMoravia.TableroDeJuego
{
    // Colores que se utilizaran en la consola
    enum Color : int
    {
        GrisOscuro = 8,
        Gris = 7,
        Amarillo = 14,
        AmarilloOscuro = 6, 
        Blanco = 15,
        Rojo = 12,    }
}