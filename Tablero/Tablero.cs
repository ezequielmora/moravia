﻿using System;

using Ajedrez.Tablero.Excepciones;

namespace AjedrezMoravia.TableroDeJuego
{   
    // Clase que construye el tablero de juego
    class Tablero
    {
        public const int Lineas = 8;
        public const int Columnas = 8;

        private Pieza[,] Piezas;

        public Tablero ()
        {
            Piezas = new Pieza[Lineas, Columnas];
        }

        public void PonerPieza (Pieza piece, Posicion posicion)
        {
            PosicionValida(posicion);

            if (PiezaExistente(posicion))
                throw new ExcepcionTablero("Ya existe una pieza en esa posicion.");

            Piezas[posicion.Linea, posicion.Columna] = piece;
            piece.CambiarPosicion(posicion);
        }

        public Pieza RemoverPieza (Posicion posicion)
        {
            if (PiezaExistente(posicion))
            {
                var pieza = ObtenerPieza(posicion);
                Piezas[posicion.Linea, posicion.Columna] = null;
                pieza.CambiarPosicion(null);

                return pieza;
            }
            else
            {
                return null;
            }
        }

        public bool PiezaExistente (Posicion posicion)
        {
            return ObtenerPieza(posicion) != null;
        }

        public Pieza ObtenerPieza (Posicion posicion)
        {
            PosicionValida(posicion);
            return Piezas[posicion.Linea, posicion.Columna];    
        }

        public bool ExisteEnemigo (Posicion posicion, Color color)
        {
            var pieza = ObtenerPieza(posicion);
            return pieza != null && pieza.Color.Equals(color);
        }

        public bool EsUnaPosicionValida (Posicion posicion)
        {
            if (posicion.Linea < 0 || posicion.Linea >= Lineas || posicion.Columna < 0 || posicion.Columna >= Columnas)
                return false;
            return true;
        }

        public void PosicionValida (Posicion posicion)
        {
            if (!EsUnaPosicionValida(posicion))
                throw new ExcepcionTablero("La posicion ingresada no es valida.");
        }
    }
}
