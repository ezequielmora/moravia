﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AjedrezMoravia.TableroDeJuego
{
    class Posicion
    {
        public int Linea {
            get;
            set;
        }
        public int Columna {
            get;
            set;
        }
    
        public Posicion (int linea, int columna)
        {
            Linea = linea;
            Columna = columna;
        }

        public override string ToString ()
        {
            return Linea + ", " + Columna;
        }
    }
}
