﻿using System;

namespace Ajedrez.Tablero.Excepciones
{
    class ExcepcionTablero : Exception
    {
        public ExcepcionTablero (string message) : base(message)
        {

        }
    }
}
