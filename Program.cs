﻿using System;

using AjedrezMoravia.TableroDeJuego;
using Ajedrez.Tablero.Excepciones;
using AjedrezMoravia.Ajedrez;
using AjedrezMoravia.Ajedrez.Excepciones;
using AjedrezMoravia;

namespace AjedrezMoravia
{
    class Program
    {
        static void Main (string[] args)
        {
            AjedrezMatch match = new AjedrezMatch();

            while (!match.Finalizado)
            {
                View.ImprimirTableroDeJuego(match.Tablero);
                View.ImprimirEstadoDelMatch(match);

                try
                {
                    Console.Write("Origen: ");
                    var posicionOrigen = View.LeerPosicionDelTablero();

                    match.ChequearPosicionDeOrigen(posicionOrigen);
                    
                    // Imprime el tablero con las posibles posiciones de la pieza
                    View.ImprimirTablero(match.Tablero, posicionOrigen);
                    View.ImprimirEstadoDelMatch(match);

                    Console.WriteLine("Origen: {0}{1}", char.ToUpper(posicionOrigen.Columna), posicionOrigen.Linea);

                    Console.Write("Destino: ");
                    var targetPosition = View.LeerPosicionDelTablero();

                    match.ChequearPosicionDeDestino(targetPosition);

                    match.EjecutarMovimiento(posicionOrigen, targetPosition);
                }
                catch (ExcepcionTablero e)
                {
                    View.ImprimirExcepcion(e);
                }
                catch (AjedrezMatchException e)
                {
                    View.ImprimirExcepcion(e);
                }
            }

            View.ImprimirFinalDelMatch(match);
        }
    }
}
