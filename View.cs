﻿using System;

using AjedrezMoravia.TableroDeJuego;
using Ajedrez.Tablero.Excepciones;
using AjedrezMoravia.Ajedrez;

namespace AjedrezMoravia
{
    static class View
    {
        private static readonly ConsoleColor _ColorPorDefecto = ConsoleColor.Gray;
        private static readonly ConsoleColor _ColorDeFondoPorDefecto = ConsoleColor.Black;

        public static void ImprimirTableroDeJuego (Tablero chessBoard)
        {
            Console.Clear();

            for (int l = 0; l < Tablero.Lineas; l++)
            {
                ImprimirEtiquetaVertical(l);

                for (int c = 0; c < Tablero.Columnas; c++)
                    ImprimirPiezaDelTablero(chessBoard.ObtenerPieza(new Posicion(l, c)), false);

                Console.WriteLine();
            }

            ImprimirEtiquetaHorizontal();

            Console.Write("\n\n");
        }

        public static void ImprimirTablero (Tablero tablero, PosicionDelTablero origen)
        {
            Console.Clear();

            for (int x = 0; x < Tablero.Lineas; x++)
            {
                ImprimirEtiquetaVertical(x);

                for (int i = 0; i < Tablero.Columnas; i++)
                {
                    var currentPosicion = new Posicion(x, i);
                    bool[,] possibleMovements = tablero.ObtenerPieza(origen.HaciaPosicion()).MovimientosPosibles();

                    if (possibleMovements[x, i])
                        ImprimirPiezaDelTablero(tablero.ObtenerPieza(currentPosicion), true);
                    else
                        ImprimirPiezaDelTablero(tablero.ObtenerPieza(currentPosicion));
                }
                Console.WriteLine();
            }

            ImprimirEtiquetaHorizontal();

            Console.Write("\n\n");
        }

        public static void ImprimirEstadoDelMatch (AjedrezMatch ajedrezMatch)
        {
            Console.WriteLine("Turno: {0}", ajedrezMatch.Turno);
            Console.WriteLine("Piezas del jugador actual: {0}", ajedrezMatch.JugadorActual);
            ImprimirPiezasFueraDeJuego(ajedrezMatch, Color.Blanco);
            ImprimirPiezasFueraDeJuego(ajedrezMatch, Color.GrisOscuro);

            if (ajedrezMatch.Check)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("JAQUE!!!");
                Console.ForegroundColor = _ColorPorDefecto;
            }

            Console.Write("\n\n");
        }

        public static void ImprimirFinalDelMatch (AjedrezMatch ajedrezMatch)
        {
            Console.Clear();

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("JAQUE MATE!!!\n");
            Console.ForegroundColor = _ColorPorDefecto;

            Console.WriteLine("Piezas del GANADOR: {0}", ajedrezMatch.JugadorActual);
            Console.WriteLine("Turno: {0}", ajedrezMatch.Turno);
            Console.WriteLine("Empezo en: {0}", ajedrezMatch.EmpiezaEn);
            Console.WriteLine("Finalizo en: {0}\n", ajedrezMatch.FinalizaEn);

            ImprimirTableroDeFinalDeJuego(ajedrezMatch.Tablero);

            Console.ReadLine();
        }

        public static PosicionDelTablero LeerPosicionDelTablero ()
        {
            var s = Console.ReadLine();

            if(s.Length.Equals(2))
            {
                if (char.IsLetter(s[0]) && char.IsNumber(s[1]))
                    return new PosicionDelTablero(char.ToLower(s[0]), int.Parse(s[1].ToString()));
                else
                    throw new ExcepcionTablero("Posicion invalida.");
            }
            else
            {
                throw new ExcepcionTablero("Posicion invalida.");
            }
        }

        public static void ImprimirExcepcion (Exception e)
        {
            Console.Clear();

            Console.ForegroundColor = (ConsoleColor)Color.Rojo;
            
            Console.WriteLine("[Error]: {0}", e.Message);
            Console.ReadLine();

            Console.ForegroundColor = _ColorPorDefecto;            
        }

        private static void ImprimirPiezaDelTablero (Pieza piece)
        {
            if (piece != null)
            {
                Console.ForegroundColor = (ConsoleColor)piece.Color;
                Console.Write(piece + " ");
                Console.ForegroundColor = _ColorPorDefecto;
            }
            else
            {
                Console.Write("- ");
            }
        }


        private static void ImprimirPiezaDelTablero (Pieza piece, bool possibleMovement)
        {
            if (possibleMovement)
                Console.BackgroundColor = (ConsoleColor)Color.AmarilloOscuro;

            if (piece != null)
            {
                Console.ForegroundColor = (ConsoleColor)piece.Color;
                Console.Write(piece + " ");
                Console.ForegroundColor = _ColorPorDefecto;
            }
            else
            {
                Console.Write("- ");
            }

            Console.BackgroundColor = _ColorDeFondoPorDefecto;
        }

        private static void ImprimirEtiquetaVertical (int line)
        {
            Console.Write(Tablero.Lineas - line + " ");
        }

        private static void ImprimirEtiquetaHorizontal ()
        {
            string label = "  ";

            for (int c = 0; c < Tablero.Columnas; c++)
                label += (char)('a' + c) + " ";

            Console.Write(label.ToUpper());
        }
    
        private static void ImprimirPiezasFueraDeJuego (AjedrezMatch ajedrezMatch, Color color)
        {
            Console.Write("Piezas fuera de juego: ");
            Console.ForegroundColor = (ConsoleColor)color;
            string value = "[";

            foreach (var piece in ajedrezMatch.ObtenerPiezasFueraDeJuego(color))
                value += (piece + " ");

            value += "]";

            Console.WriteLine(value);
            Console.ForegroundColor = _ColorPorDefecto;
        }

        private static void ImprimirTableroDeFinalDeJuego (Tablero tablero)
        {
            for (int l = 0; l < Tablero.Lineas; l++)
            {
                ImprimirEtiquetaVertical(l);

                for (int c = 0; c < Tablero.Columnas; c++)
                    ImprimirPiezaDelTablero(tablero.ObtenerPieza(new Posicion(l, c)), false);

                Console.WriteLine();
            }

            ImprimirEtiquetaHorizontal();

            Console.Write("\n\n");
        }
    }
}
